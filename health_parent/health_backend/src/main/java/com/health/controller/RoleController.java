package com.health.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.health.constant.MessageConstant;
import com.health.entity.PageResult;
import com.health.entity.QueryPageBean;
import com.health.entity.Result;
import com.health.pojo.Role;
import com.health.service.RoleService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping ("/role")
public class RoleController {
    @Reference
    private RoleService roleService;

    /**
     * 查询所有角色
     * @return
     */
    @RequestMapping("/findAll")
    public Result findAll(){
        try {
            List<Role> list = roleService.findAll();
            return new Result(true, MessageConstant.QUERY_ROLE_SUCCESS,list);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.QUERY_ROLE_FAIL );
        }

    }

    /**
     * 根据用户订单查询响应的角色
     * @param userId
     * @return
     */
    @RequestMapping("/findRoleIdsByUserId")
    public Result findRoleIdsByUserId(Integer userId){
        try {
            List<Integer> list = roleService.findRoleIdsByUserId(userId);
            return new Result(true, MessageConstant.QUERY_ROLE_SUCCESS,list);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.QUERY_ROLE_FAIL );
        }
    }

    /**
     * 根据条件查询角色
     * @param queryPageBean
     * @return
     */
    @RequestMapping("/queryPage")
    public PageResult queryPage(@RequestBody QueryPageBean queryPageBean){
        PageResult pageResult = roleService.findRoleByCondition(queryPageBean);
        return pageResult;
    }

    /**
     * 添加角色
     * @param role
     * @return
     */
    @RequestMapping("/add")
    public Result add(@RequestBody Role role){
        try {
            roleService.add(role);
            return new Result(true, MessageConstant.ADD_ROLE_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.ADD_ROLE_FAIL);
        }
    }

    /**
     * 更新权限
     * @param permissionIds
     * @param roleId
     * @return
     */
    @RequestMapping("/updatePermissions")
    public Result updatePermissions (@RequestBody List<Integer> permissionIds,Integer roleId){
        try {
            roleService.updatePermissions(permissionIds,roleId);
            return new Result(true,MessageConstant.EDIT_PERMISSION_SUCCESS );
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.EDIT_PERMISSION_FAIL);
        }
    }

    /**
     * 根据id删除角色
     * @param roleId
     * @return
     */
    @RequestMapping("/deleteRole")
    public Result deleteRole(Integer roleId){
        try {
            roleService.deleteRole(roleId);
            return new Result(true, MessageConstant.DELETE_ROLE_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.DELETE_ROLE_FAIL);
        }
    }
}
