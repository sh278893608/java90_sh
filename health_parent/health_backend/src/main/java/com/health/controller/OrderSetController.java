package com.health.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.health.constant.MessageConstant;
import com.health.entity.PageResult;
import com.health.entity.QueryPageBean;
import com.health.entity.Result;
import com.health.pojo.OrderSetting;
import com.health.service.OrderSetService;
import com.health.utils.POIUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@RestController
@RequestMapping("/ordersetting")
public class OrderSetController {

    @Reference
    private OrderSetService orderSetService;

    @RequestMapping("/upload")
    public Result upload(@RequestParam("excelFile") MultipartFile excelFile) {
        try {
            List<String[]> listStr = POIUtils.readExcel(excelFile);
            List<OrderSetting> orderSetList = new ArrayList<>();
            for (String[] strings : listStr) {
                String orderDate = strings[0];
                String number = strings[1];
                OrderSetting orderSetting = new OrderSetting(new Date(orderDate), Integer.parseInt(number));
                orderSetList.add(orderSetting);
            }
            orderSetService.add(orderSetList);
            return new Result(true, MessageConstant.IMPORT_ORDERSETTING_SUCCESS);
        } catch (IOException e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.IMPORT_ORDERSETTING_FAIL);
        }
    }

    @RequestMapping("/getOrderSetsByMouth")
    public Result getOrderSetsByMouth(String date) {
        try {
            List<Map> orderSetsList = orderSetService.getOrderSetsByMouth(date);
            return new Result(true, MessageConstant.GET_ORDERSETTING_SUCCESS, orderSetsList);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.GET_ORDERSETTING_FAIL);
        }
    }

    @RequestMapping("/editOne")
    public Result editOne(@RequestBody OrderSetting orderSetting) {
        try {
            orderSetService.editOne(orderSetting);
            return new Result(true, "编辑预约设定成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "编辑预约设定失败");
        }
    }

}
