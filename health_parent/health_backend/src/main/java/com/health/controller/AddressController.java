package com.health.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.health.entity.PageResult;
import com.health.entity.QueryPageBean;
import com.health.entity.Result;
import com.health.pojo.Address;
import com.health.service.AddressService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/address")
public class AddressController {

    @Reference
    private AddressService addressService;


    //查询所有地址
    //检查项分页查询
    @RequestMapping("/findAll")
    public PageResult findAll(@RequestBody QueryPageBean queryPageBean) {
        try {
            return addressService.findAll(queryPageBean);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //新增地址
    @RequestMapping("/add")
    public Result add(@RequestBody Address address){

        try {
            addressService.add(address);
            return new Result(true,"添加地址成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"添加地址失败");
        }
    }





    //删除地址
    @RequestMapping("/del")
    public  Result del(Integer id){
        try {
            addressService.del(id);
            return new Result(true,"删除地址成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"删除地址失败");
        }
    }
}
