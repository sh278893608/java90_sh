package com.health.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.health.constant.MessageConstant;
import com.health.entity.PageResult;
import com.health.entity.QueryPageBean;
import com.health.entity.Result;
import com.health.service.UserService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

@RestController
@RequestMapping("/user")
public class UserController {

    @Reference
    private UserService userService;

    /**
     * 获取用户名，用于展示到页面
     * @return
     */
    @RequestMapping("/getUsername")
    public Result getUsername() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (user != null) {
            return new Result(true, MessageConstant.GET_USERNAME_SUCCESS, user.getUsername());
        }
        return new Result(false, MessageConstant.GET_USERNAME_FAIL);
    }

    /**
     * 根据条件查询用户
     * @param queryPageBean 页面查询信息
     * @return
     */
    @RequestMapping("/queryPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean) {
        PageResult queryPage = userService.queryPage(queryPageBean);
        return queryPage;
    }

    /**
     * 新建用户
     * @param user
     * @return
     */
    @RequestMapping("/add")
    public Result add(@RequestBody com.health.pojo.User user){
        try {
            userService.add(user);
            return new Result(true, MessageConstant.ADD_MEMBER_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.ADD_MEMBER_FAIL);
        }
    }

    /**
     * 更新用户绑定的角色
     */
    @RequestMapping("/updateRoles")
    public Result updateRoles(@RequestBody Integer[] roleIds,Integer userId){
        System.out.println(Arrays.toString(roleIds));
        System.out.println(userId);
        try {
            userService.updateRoles(userId,roleIds);
            return new Result(true,MessageConstant.EDIT_ROLE_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.EDIT_ROLE_FAIL);
        }
    }

    /**
     * 根据用户id删除用户
     * @param userId 用户id
     * @return
     */
    @RequestMapping("/deleteUser")
    public Result deleteUser(Integer userId){
        try {
            userService.deleteUser(userId);
            return new Result(true,MessageConstant.DELETE_USER_SUCCESS );
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.DELETE_USER_FAIL);
        }
    }
}
