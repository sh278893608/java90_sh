package com.health.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.health.constant.MessageConstant;
import com.health.entity.PageResult;
import com.health.entity.QueryPageBean;
import com.health.entity.Result;
import com.health.pojo.Order;
import com.health.service.OrderService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/order")
public class OrderController {
    @Reference
    private OrderService orderService;

    //根据条件查询预约记录
    @RequestMapping("/findOrdersPage")
    public PageResult findOrdersPage(@RequestBody QueryPageBean queryPageBean) {
        PageResult pageResult = orderService.findOrdersByCondition(queryPageBean);
        return pageResult;
    }

    //确认到诊
    @RequestMapping("/confirm")
    public Result confirm(Integer id){
        try {
            orderService.confirm(id);
            return new Result(true, MessageConstant.ORDER_CONFIRM_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.ORDER_CONFIRM_FAIL);
        }
    }

    //取消预约
    @RequestMapping("/cancel")
    public Result cancel(Integer id){
        try {
            orderService.cancel(id);
            return new Result(true, MessageConstant.ORDER_CANCEL_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.ORDER_CANCEL_FAIL);
        }
    }
    //新建预约
    @RequestMapping("/add")
    public Result add(@RequestBody Map<String,Object> map,String setmealId) throws Exception {
        System.out.println(setmealId);
        map.put("orderType", Order.ORDERTYPE_TELEPHONE);
        map.put("setmealId", setmealId);
        Result result = orderService.addOrder(map);
        return result;
    }

    //批量确认就诊
    @RequestMapping("/batchConfirm")
    public Result batchConfirm(@RequestBody List<Map<String,Object>> list){
        try {
            //遍历集合获取id
            List<Integer> orderIds = new ArrayList<>();
            for (Map<String, Object> map : list) {
                Integer id = (Integer) map.get("id");
                orderIds.add(id);
            }
            orderService.batchConfirm(orderIds);
            return new Result(true, MessageConstant.ORDER_BATCHCONFIRM_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.ORDER_BATCHCONFIRM_FAIL);
        }
    }

    //批量取消预约
    @RequestMapping("/batchCancel")
    public Result batchCancel(@RequestBody List<Map<String,Object>> list){
        try {
            //遍历集合获取id
            List<Integer> orderIds = new ArrayList<>();
            for (Map<String, Object> map : list) {
                Integer id = (Integer) map.get("id");
                orderIds.add(id);
            }
            orderService.batchCancel(orderIds);
            return new Result(true, MessageConstant.ORDER_BATCHCANCEL_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.ORDER_BATCHCANCEL_FAIL);
        }
    }

}
