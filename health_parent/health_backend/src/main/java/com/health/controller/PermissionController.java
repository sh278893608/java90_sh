package com.health.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.health.constant.MessageConstant;
import com.health.entity.PageResult;
import com.health.entity.QueryPageBean;
import com.health.entity.Result;
import com.health.pojo.Permission;
import com.health.service.PermissionService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController()
@RequestMapping("/permission")
public class PermissionController {

    @Reference
    private PermissionService permissionService;

    /**
     * 根据条件查询权限
     * @param queryPageBean
     * @return
     */
    @RequestMapping("/queryPage")
    public PageResult queryPage(@RequestBody QueryPageBean queryPageBean){
        try {
            PageResult pageResult = permissionService.queryPage(queryPageBean);
            return pageResult;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 添加权限
     * @param permission
     * @return
     */
    @RequestMapping("/add")
    public Result add(@RequestBody Permission permission){
        try {
            permissionService.add(permission);
            return new Result(true,"权限添加成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"权限添加失败");
        }
    }

    /**
     * 根据id删除权限
     * @param id
     * @return
     */
    @RequestMapping("/delete")
    public Result delete(Integer id){
        try {
            permissionService.delete(id);
            return new Result(true,"权限删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"权限删除失败");
        }
    }

    /**
     * 查询单个权限信息
     * @param id
     * @return
     */
    @RequestMapping("/findOne")
    public Result findOne(Integer id){
        try {
            Permission permission = permissionService.findOne(id);
            return new Result(true,"权限信息展示成功",permission);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"权限信息展示失败");
        }
    }

    /**
     * 编辑权限
     * @param permission
     * @return
     */
    @RequestMapping("/edit")
    public Result edit(@RequestBody Permission permission){
        try {
            permissionService.edit(permission);
            return new Result(true,"权限修改成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"权限修改失败");
        }
    }

    /**
     * 查询所有的角色
     * @return
     */
    @RequestMapping("/findAll")
    public Result findAll(){
        try {
            List<Permission> list = permissionService.findAll();
            return new Result(true, MessageConstant.QUERY_PERMISSION_SUCCESS,list);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_PERMISSION_FAIL);
        }
    }

    /**
     * 根据角色id查询当前所有的角色，用于回显
     * @param roleId
     * @return
     */
    @RequestMapping("/findPermissionIdsByRoleId")
    public Result findPermissionIdsByRoleId(Integer roleId){
        try {
            List<Integer> list = permissionService.findPermissionIdsByRoleId(roleId);
            return new Result(true, MessageConstant.QUERY_PERMISSION_SUCCESS,list);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_PERMISSION_FAIL);
        }
    }
}
