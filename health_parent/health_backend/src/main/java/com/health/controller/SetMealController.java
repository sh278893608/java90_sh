package com.health.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.health.constant.MessageConstant;
import com.health.constant.RedisConstant;
import com.health.entity.PageResult;
import com.health.entity.QueryPageBean;
import com.health.entity.Result;
import com.health.pojo.Setmeal;
import com.health.service.SetMealService;
import com.health.utils.QiniuUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import redis.clients.jedis.JedisPool;


import java.io.IOException;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/setmeal")
public class SetMealController {

    @Reference
    private SetMealService setMealService;

    @Autowired
    private JedisPool jedisPool;

    @RequestMapping("/upload")
    public Result upload(@RequestParam("imgFile") MultipartFile imgFile){
        //构造文件名
        String originalFilename = imgFile.getOriginalFilename();
        int index = originalFilename.lastIndexOf(".");
        String extension = originalFilename.substring(index);
        String fileName = UUID.randomUUID().toString()+extension;
        //上传七牛云并响应前端
        try {
            QiniuUtils.upload2Qiniu(imgFile.getBytes(),fileName);
            jedisPool.getResource().sadd(RedisConstant.SETMEAL_PIC_RESOURCES,fileName);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.PIC_UPLOAD_FAIL);
        }
        return new Result(true,MessageConstant.PIC_UPLOAD_SUCCESS,fileName);
    }

    @RequestMapping("/add")
    public Result add(@RequestBody Setmeal setmeal,Integer[] checkgroupIds){
        try {
            setMealService.add(setmeal,checkgroupIds);
            return new Result(true,MessageConstant.ADD_SETMEAL_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.ADD_SETMEAL_FAIL);
        }
    }

    @RequestMapping("/findAll")
    public PageResult findAll(@RequestBody QueryPageBean queryPageBean){
        PageResult pageResult = setMealService.findAll(queryPageBean);
        return pageResult;
    }

    @RequestMapping("/confirmRel")
    public Result confirmRel(Integer setmealId){
        try {
            boolean flag = setMealService.confirmRel(setmealId);
            return new Result(true,"删除套餐成功",flag);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"删除套餐失败");
        }
    }

    @RequestMapping("/delete")
    public Result delete(Integer setmealId){
        try {
            setMealService.delete(setmealId);
            return new Result(true,"删除套餐成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"删除套餐失败");
        }
    }

    @RequestMapping("/findAllSetmeal")
    public Result findAllSetmeal(){
        try {
            List<Setmeal> list = setMealService.findAllSetmeal();
            return new Result(true, MessageConstant.QUERY_SETMEALLIST_SUCCESS,list);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_SETMEALLIST_FAIL);
        }
    }
}
