package com.health.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.health.entity.Result;
import com.health.pojo.Address;
import com.health.service.AddressService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/address")
public class AddressController {

    @Reference
    private AddressService addressService;

    /**
     * 查询全部地址信息
     * @return
     */
    @RequestMapping("/findAll")
    public Result findAllAddr(){
        try {
            List<String> addressList = addressService.findAllAddr();
            return new Result(true,"地址信息查询成功",addressList);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"地址信息查询失败");
        }
    }


}
