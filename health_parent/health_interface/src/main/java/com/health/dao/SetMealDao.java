package com.health.dao;

import com.github.pagehelper.Page;
import com.health.pojo.Setmeal;

import java.util.List;
import java.util.Map;

public interface SetMealDao {
    void add(Setmeal setmeal);

    void setMealGroup(Map<String, Integer> map);

    Page<Setmeal> findAll(String queryString);

    Integer confirmRel(Integer setmealId);

    Setmeal findById(Integer setmealId);

    void deleteRel(Integer setmealId);

    void delete(Integer setmealId);

    List<Setmeal> findAllMob();

    List<Map<String,Object>> findSetmealCount();

    List<Setmeal> findAllSetmeal();
}
