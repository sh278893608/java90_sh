package com.health.dao;

import com.github.pagehelper.Page;
import com.health.entity.PageResult;
import com.health.entity.QueryPageBean;
import com.health.pojo.Address;

import java.util.List;


public interface AddressDao {

    //新增地址
    void add(Address address);

    //删除地址
    void del(Integer id);

    //分页查询


    //根据条件查询地址列表
    Page<Address> selectByCondition(String queryString);

    //查询全部地址
    List<String> findAllAddr();
}
