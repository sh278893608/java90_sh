package com.health.dao;


import com.github.pagehelper.Page;
import com.health.pojo.Order;

import java.util.List;
import java.util.Map;

public interface OrderDao {
    public void add(Order order);

    public List<Order> findByCondition(Order order);

    public Map findById4Detail(Integer id);

    public Integer findOrderCountByDate(String date);

    public Integer findOrderCountAfterDate(String date);

    public Integer findVisitsCountByDate(String date);

    public Integer findVisitsCountAfterDate(String date);

    public List<Map> findHotSetmeal();

    /**
     * 根据条件查询订单列表
     * @param queryString
     * @return
     */
    Page<Map<String, Object>> findOrdersByCondition(String queryString);

    /**
     * 确认到诊
     * @param id
     */
    void confirm(Integer id);

    /**
     * 取消预约
     * @param id
     */
    void cancel(Integer id);
}
