package com.health.dao;

import com.github.pagehelper.Page;
import com.health.pojo.OrderSetting;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface OrderSetDao {
    Long findByOrderDate(Date orderDate);

    void editNumByOrderDate(OrderSetting orderSetting);

    void add(OrderSetting orderSetting);

    List<OrderSetting> getOrderSetsByMouth(Map<String, String> conditionMap);

    OrderSetting findOrderSettingByorderDate(Date date);


    void editReservationsByOrderdate(Date date);


}
