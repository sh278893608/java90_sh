package com.health.dao;

import com.github.pagehelper.Page;
import com.health.pojo.User;

import java.util.Map;


public interface UserDao {
    /**
     * 根据用户名查询用户信息
     * @param username
     * @return
     */
    User findByUsername(String username);

    /**
     * 根据条件查询用户列表
     * @param queryString
     * @return
     */
    Page<User> selectByCondition(String queryString);

    /**
     * 添加用户
     * @param user
     */
    void add(User user);

    /**
     * 根据id删除角色
     * @param userId
     */
    void deleteRoles(Integer userId);

    /**
     * 根据条件更新角色
     * @param map
     */
    void updateRoles(Map<String,Integer> map);

    /**
     * 根据用户id删除用户
     * @param userId
     */
    void deleteUser(Integer userId);
}
