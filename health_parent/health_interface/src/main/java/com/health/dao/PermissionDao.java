package com.health.dao;

import com.github.pagehelper.Page;
import com.health.pojo.Permission;

import java.util.List;
import java.util.Set;

public interface PermissionDao {
    /**
     * 根据角色id查询权限信息
     * @param roleId
     * @return
     */
    Set<Permission> findByRoleId(Integer roleId);

    /**
     * 根据条件查询权限列表
     * @param queryString
     * @return
     */
    Page<Permission> findByQueryString(String queryString);

    /**
     * 添加权限
     * @param permission
     */
    void add(Permission permission);

    /**
     * 根据角色id查询是否有权限
     * @param id
     * @return
     */
    Long findRoleCountById(Integer id);

    /**
     * 根据权限id删除权限
     * @param id
     */
    void delete(Integer id);

    /**
     * 根据权限id查询权限信息，用于编辑权限
     * @param id
     * @return
     */
    Permission findOne(Integer id);

    /**
     * 编辑权限
     * @param permission
     */
    void edit(Permission permission);

    /**
     * 查询全部权限信息
     * @return
     */
    List<Permission> findAll();

    /**
     * 根据角色id查询角色拥有的权限id，用于回显
     * @param roleId
     * @return
     */
    List<Integer> findPermissionIdsByRoleId(Integer roleId);
}
