package com.health.dao;

import com.github.pagehelper.Page;
import com.health.pojo.CheckGroup;

import java.util.List;
import java.util.Map;

public interface CheckGroupDao {
    void add(CheckGroup checkGroup);

    void setGroupItemRel(Map<String, Integer> map);

    Page<CheckGroup> queryPage(String queryString);

    CheckGroup findById(Integer id);

    List<Integer> findCheckItemIdsByCheckGroupId(Integer checkgroup_id);

    void edit(CheckGroup checkGroup);

    void deleteRel(Integer checkGroupId);

    Long confirmRel(Integer checkGroupId);

    void delete(Integer checkGroupId);

    List<CheckGroup> findAll();

    List<Integer> getCgIds(Integer id);
}
