package com.health.dao;

import com.github.pagehelper.Page;
import com.health.pojo.Role;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface RoleDao {
    /**
     * 根据用户id查询角色
     * @param userId
     * @return
     */
    Set<Role> findByUserId(Integer userId);

    /**
     * 查询全部角色
     * @return
     */
    List<Role> findAll();

    /**
     * 根据用户id查询拥有的角色id，用于编辑角色回显数据
     * @param UserId
     * @return
     */
    List<Integer> findRoleIdsByUserId(Integer UserId);

    /**
     * 根据用户id先删除角色，用于删除用户
     * @param userId
     */
    void deleteByUserId(Integer userId);

    /**
     * 根据用户id查询是否拥有角色
     * @param userId
     * @return
     */
    Long findUserIdAndRoleId(Integer userId);

    /**
     * 根据条件查询角色列表
     * @param queryString
     * @return
     */
    Page<Role> findRoleByCondition(String queryString);

    /**
     * 添加角色
     * @param role
     */
    void add(Role role);

    /**
     * 根据角色id查询是否拥有权限
     * @param roleId
     * @return
     */
    Long findPermissionCountByRoleId(Integer roleId);

    /**
     * 根据角色id删除权限
     * @param roleId
     */
    void deletePermissionByRoleId(Integer roleId);

    /**
     * 映射添加权限
     * @param map
     */
    void addPermission(Map<String, Integer> map);

    /**
     * 根据角色id删除角色
     * @param roleId
     */
    void deleteRole(Integer roleId);
}
