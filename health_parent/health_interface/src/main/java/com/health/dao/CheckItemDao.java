package com.health.dao;

import com.github.pagehelper.Page;
import com.health.pojo.CheckItem;
import org.apache.ibatis.annotations.Insert;

import java.util.List;

public interface CheckItemDao {

    List<Integer> getCiIds(Integer cgId);

    void add(CheckItem checkItem);

    Page<CheckItem> selectByCondition(String queryString);

    Long findByCheckItemId(Integer id);

    void delete(Integer id);

    CheckItem findOne(Integer id);

    void update(CheckItem checkItem);

    List<CheckItem> findAll();

}
