package com.health.service;

import com.health.pojo.OrderSetting;

import java.util.List;
import java.util.Map;

public interface OrderSetService {
    void add(List<OrderSetting> orderSetList);

    List<Map> getOrderSetsByMouth(String date);

    void editOne(OrderSetting orderSetting);

}
