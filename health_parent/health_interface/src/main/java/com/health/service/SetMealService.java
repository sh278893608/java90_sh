package com.health.service;

import com.health.entity.PageResult;
import com.health.entity.QueryPageBean;
import com.health.pojo.Setmeal;

import java.util.List;
import java.util.Map;

public interface SetMealService {
    void add(Setmeal setmeal, Integer[] checkgroupIds);

    PageResult findAll(QueryPageBean queryPageBean);

    boolean confirmRel(Integer setmealId);

    void delete(Integer setmealId);

    List<Setmeal> findAllMob();

    Setmeal findSetmealById(Integer id);

    List<Map<String,Object>> findSetmealCount();

    List<Setmeal> findAllSetmeal();
}
