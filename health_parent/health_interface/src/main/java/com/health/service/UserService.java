package com.health.service;

import com.health.entity.PageResult;
import com.health.entity.QueryPageBean;
import com.health.pojo.User;

public interface UserService {
    /**
     * 获取用户名，用于展示到页面
     * @return
     */
    User findByUsername(String username);

    /**
     * 根据条件查询所有的用户
     * @param queryPageBean
     * @return
     */
    PageResult queryPage(QueryPageBean queryPageBean);

    /**
     * 添加用户
     * @param user
     */
    void add(User user);

    /**
     * 更新用户所对应的角色
     * @param userId
     * @param roleIds
     */
    void updateRoles(Integer userId, Integer[] roleIds);

    /**
     * 删除用户
     * @param userId
     */
    void deleteUser(Integer userId);
}
