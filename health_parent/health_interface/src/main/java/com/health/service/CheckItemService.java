package com.health.service;

import com.health.entity.PageResult;
import com.health.entity.QueryPageBean;
import com.health.pojo.CheckItem;

import java.util.List;

public interface CheckItemService {
    void add(CheckItem checkItem);

    PageResult queryPage(QueryPageBean queryPageBean);

    void delete(Integer id);

    CheckItem findOne(Integer id);

    void edit(CheckItem checkItem);

    List<CheckItem> findAll();

}
