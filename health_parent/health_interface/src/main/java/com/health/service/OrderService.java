package com.health.service;

import com.health.entity.PageResult;
import com.health.entity.QueryPageBean;
import com.health.entity.Result;

import java.util.List;
import java.util.Map;

public interface OrderService {
    Result addOrder(Map map) throws Exception;

    /**
     * 根据id查询当前订单，用于微信端回显预约成功
     * @param id
     * @return
     */
    Map findById(Integer id);

    /**
     * 根据条件查询预约订单
     * @param queryPageBean
     * @return
     */
    PageResult findOrdersByCondition(QueryPageBean queryPageBean);

    /**
     * 确认到诊
     * @param id
     */
    void confirm(Integer id);

    /**
     * 取消预约
     * @param id
     */
    void cancel(Integer id);

    /**
     * 批量确认
     * @param orderIds
     */
    void batchConfirm(List<Integer> orderIds);

    /**
     * 批量取消预约
     * @param orderIds
     */
    void batchCancel(List<Integer> orderIds);
}
