package com.health.service;

import com.health.entity.PageResult;
import com.health.entity.QueryPageBean;
import com.health.pojo.CheckGroup;

import java.util.List;

public interface CheckGroupService {
    void add(CheckGroup checkGroup, Integer[] checkItemIds);

    PageResult queryPage(QueryPageBean queryPageBean);

    CheckGroup findById(Integer id);

    List<Integer> findCheckItemIdsByCheckGroupId(Integer checkgroup_id);

    void edit(CheckGroup checkGroup, Integer[] checkitemIds);

    boolean confirmRel(Integer checkGroupId);

    void deleteRel(Integer checkGroupId);

    List<CheckGroup> findAll();
}
