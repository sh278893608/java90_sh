package com.health.service;

import com.health.entity.PageResult;
import com.health.entity.QueryPageBean;
import com.health.pojo.Permission;

import java.util.List;

public interface PermissionService {
    /**
     * 根据条件查询权限信息
     * @param queryPageBean
     * @return
     */
    PageResult queryPage(QueryPageBean queryPageBean);

    /**
     * 新增权限
     * @param permission
     */
    void add(Permission permission);

    /**
     * 删除权限
     * @param id
     */
    void delete(Integer id);

    /**
     * 根据id查询权限
     * @param id
     * @return
     */
    Permission findOne(Integer id);

    /**
     * 编辑权限
     * @param permission
     */
    void edit(Permission permission);

    /**
     * 查询所有权限
     * @return
     */
    List<Permission> findAll();

    /**
     * 根据角色ID查询权限ID
     * @param roleId
     * @return
     */
    List<Integer> findPermissionIdsByRoleId(Integer roleId);
}
