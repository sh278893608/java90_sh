package com.health.service;

import com.health.entity.PageResult;
import com.health.entity.QueryPageBean;
import com.health.pojo.Address;

import java.util.List;

public interface AddressService {

    //新增地址
    void add(Address address);

    //删除地址
    void del(Integer id);

    //分页查询
    PageResult findAll(QueryPageBean queryPageBean);

    //查询体检地址
    List<String> findAllAddr();
}
