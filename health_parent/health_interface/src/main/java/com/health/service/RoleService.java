package com.health.service;

import com.health.entity.PageResult;
import com.health.entity.QueryPageBean;
import com.health.pojo.Role;

import java.util.List;

public interface RoleService {

    /**
     * 查询所有角色
     * @return
     */
    public List<Role> findAll();

    /**
     * 根据用户id查询角色
     * @param userId
     * @return
     */
    List<Integer> findRoleIdsByUserId(Integer userId);

    /**
     * 根据条件查询角色信息
     * @param queryPageBean
     * @return
     */
    PageResult findRoleByCondition(QueryPageBean queryPageBean);

    /**
     * 添加角色
     * @param role
     */
    void add(Role role);

    /**
     * 更新角色关联的权限信息
     * @param permissionIds
     * @param roleId
     */
    void updatePermissions(List<Integer> permissionIds, Integer roleId);

    /**
     * 根据角色id删除角色
     * @param roleId
     */
    void deleteRole(Integer roleId);
}
