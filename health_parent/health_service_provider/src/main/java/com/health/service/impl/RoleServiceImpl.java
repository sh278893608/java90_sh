package com.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.health.dao.PermissionDao;
import com.health.dao.RoleDao;
import com.health.entity.PageResult;
import com.health.entity.QueryPageBean;
import com.health.pojo.Role;
import com.health.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

@Service(interfaceClass = RoleService.class)
@Transactional
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleDao roleDao;

    @Override
    public List<Role> findAll() {
        List<Role> roleList = roleDao.findAll();
        return roleList;
    }

    @Override
    public List<Integer> findRoleIdsByUserId(Integer userId) {
        return roleDao.findRoleIdsByUserId(userId);
    }

    @Override
    public PageResult findRoleByCondition(QueryPageBean queryPageBean) {
        //获取当前页、每页条数、查询的关键字
        Integer currentPage = queryPageBean.getCurrentPage();
        Integer pageSize = queryPageBean.getPageSize();
        String queryString = queryPageBean.getQueryString();

        //设置分页条件
        PageHelper.startPage(currentPage, pageSize);
        Page<Role> page = roleDao.findRoleByCondition(queryString);

        //返回结果
        return new PageResult(page.getTotal(), page.getResult());
    }

    @Override
    public void add(Role role) {
        roleDao.add(role);
    }

    @Override
    public void updatePermissions(List<Integer> permissionIds, Integer roleId) {
        deletePermission(roleId);
        //再遍历插入到角色权限表
        for (Integer permissionId : permissionIds) {
            HashMap<String, Integer> map = new HashMap<>();
            map.put("roleId",roleId );
            map.put("permissionId", permissionId);
            roleDao.addPermission(map);

        }
    }

    private void deletePermission(Integer roleId) {
        //先查询角色是否有关联的权限
        Long count = roleDao.findPermissionCountByRoleId(roleId);
        if (count>0){
            //有关联的权限，则先删除
            roleDao.deletePermissionByRoleId(roleId);
        }
    }

    @Override
    public void deleteRole(Integer roleId) {
        deletePermission(roleId);
        roleDao.deleteRole(roleId);
    }


}
