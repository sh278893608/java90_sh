package com.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.health.constant.MessageConstant;
import com.health.dao.MemberDao;
import com.health.dao.OrderDao;
import com.health.dao.OrderSetDao;
import com.health.entity.PageResult;
import com.health.entity.QueryPageBean;
import com.health.entity.Result;
import com.health.pojo.Member;
import com.health.pojo.Order;
import com.health.pojo.OrderSetting;
import com.health.service.OrderService;
import com.health.utils.DateUtils;
import org.apache.zookeeper.data.Id;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = OrderService.class)
@Transactional
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDao orderDao;
    @Autowired
    private OrderSetDao orderSetDao;
    @Autowired
    private MemberDao memberDao;

    @Override
    public Result addOrder(Map map) throws Exception{
        //1.判断后台是否对预约日期这一天已预约设置
        String orderDate = (String)map.get("orderDate");
        Date date = DateUtils.parseString2Date(orderDate);
        OrderSetting orderSetting = orderSetDao.findOrderSettingByorderDate(date);
        if (orderSetting==null){
            return new Result(false,MessageConstant.SELECTED_DATE_CANNOT_ORDER);
        }
        // 2.预约人数是否已满
        int number = orderSetting.getNumber();//可预约人数
        int reservations = orderSetting.getReservations();//已预约人数
        if (reservations>=number){
            return new Result(false,MessageConstant.ORDER_FULL);
        }
        // 3.在某一天是否已经预约了这个套餐
        String telephone = (String)map.get("telephone");
        Member member = memberDao.findByTelephone(telephone);
        if (member!=null){
            Integer memberId = member.getId();
            String setmealId = (String)map.get("setmealId");
            Order order = new Order(memberId,date,Integer.parseInt(setmealId));
            List<Order> orderList = orderDao.findByCondition(order);
            if (orderList!=null && orderList.size()>0){
                return new Result(false,MessageConstant.HAS_ORDERED);
            }
        }else {
            // 4.用户是否是会员，若不是会员，自动注册
            member = new Member();
            //  `name` varchar(32) DEFAULT NULL,
            member.setName((String)map.get("name"));
            //  `sex` varchar(8) DEFAULT NULL,
            member.setSex((String) map.get("sex"));
            //  `idCard` varchar(18) DEFAULT NULL,
            member.setIdCard((String)map.get("idCard"));
            //  `phoneNumber` varchar(11) DEFAULT NULL,
            member.setPhoneNumber((String) map.get("telephone"));
            //  `regTime` date DEFAULT NULL,
            member.setRegTime(new Date());
            memberDao.add(member);
        }
        // 5.添加已预约人数
        Order order = new Order();
        order.setMemberId(member.getId());
        order.setOrderDate(date);
        order.setOrderType((String) map.get("orderType"));
        order.setOrderStatus(Order.ORDERSTATUS_NO);
        order.setSetmealId(Integer.parseInt((String)map.get("setmealId")));
        order.setAddr((String) map.get("addr"));
        orderDao.add(order);
        //修改已预约人数
        orderSetDao.editReservationsByOrderdate(date);
        System.out.println("尊敬的用户，恭喜您预约成功"+map.get("name")+"套餐，请于"+map.get("orderDate")+"及时到达相应体检机构体检。");
        return new Result(true,MessageConstant.ORDER_SUCCESS,order.getId());
    }

    @Override
    public Map findById(Integer id) {
        Map map = orderDao.findById4Detail(id);
        return map;
    }

    @Override
    public PageResult findOrdersByCondition(QueryPageBean queryPageBean) {
        //获取当前页，每页显示条数，查询的关键字
        Integer currentPage = queryPageBean.getCurrentPage();
        Integer pageSize = queryPageBean.getPageSize();
        String queryString = queryPageBean.getQueryString();

        PageHelper.startPage(currentPage, pageSize);
        Page<Map<String, Object>> page = orderDao.findOrdersByCondition(queryString);
        System.out.println(page);
        PageResult pageResult = new PageResult(page.getTotal(), page);

        return pageResult;
    }

    @Override
    public void confirm(Integer id) {
        orderDao.confirm(id);
    }

    @Override
    public void cancel(Integer id) {
        orderDao.cancel(id);
    }

    @Override
    public void batchConfirm(List<Integer> orderIds) {
        for (Integer orderId : orderIds) {
            this.confirm(orderId);
        }
    }

    @Override
    public void batchCancel(List<Integer> orderIds) {
        for (Integer orderId : orderIds) {
            this.cancel(orderId);
        }
    }
}
