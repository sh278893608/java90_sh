package com.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.health.dao.MemberDao;
import com.health.dao.OrderDao;
import com.health.dao.SetMealDao;
import com.health.service.ReportService;
import com.health.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = ReportService.class)
@Transactional
public class ReportServiceImpl implements ReportService {

    @Autowired
    private MemberDao memberDao;

    @Autowired
    private SetMealDao setMealDao;

    @Autowired
    private OrderDao orderDao;

    @Override
    public Map<String, Object> getBusinessReportData() throws Exception {
        Map<String,Object> data = new HashMap<>();
        //日期
        String today = DateUtils.parseDate2String(new Date());
        String monday = DateUtils.parseDate2String(DateUtils.getThisWeekMonday());
        String firstDayThisMonth = DateUtils.parseDate2String(DateUtils.getFirstDay4ThisMonth());
        //reportDate:null,
        data.put("reportDate",today);
        //todayNewMember :0,
        Integer todayNewMember = memberDao.findMemberCountByDate(today);
        data.put("todayNewMember",todayNewMember);
        //totalMember :0,
        Integer totalMember = memberDao.findMemberTotalCount();
        data.put("totalMember",totalMember);
        //thisWeekNewMember :0,
        Integer thisWeekNewMember = memberDao.findMemberCountAfterDate(monday);
        data.put("thisWeekNewMember",thisWeekNewMember);
        //thisMonthNewMember :0,
        Integer thisMonthNewMember = memberDao.findMemberCountAfterDate(firstDayThisMonth);
        data.put("thisMonthNewMember",thisMonthNewMember);
        //todayOrderNumber :0,
        Integer todayOrderNumber = orderDao.findOrderCountByDate(today);
        data.put("todayOrderNumber",todayOrderNumber);
        //todayVisitsNumber :0,
        Integer todayVisitsNumber = orderDao.findVisitsCountByDate(today);
        data.put("todayVisitsNumber",todayVisitsNumber);
        //thisWeekOrderNumber :0,
        Integer thisWeekOrderNumber = orderDao.findOrderCountAfterDate(monday);
        data.put("thisWeekOrderNumber",thisWeekOrderNumber);
        //thisWeekVisitsNumber :0,
        Integer thisWeekVisitsNumber = orderDao.findVisitsCountAfterDate(monday);
        data.put("thisWeekVisitsNumber",thisWeekVisitsNumber);
        //thisMonthOrderNumber :0,
        Integer thisMonthOrderNumber = orderDao.findOrderCountAfterDate(firstDayThisMonth);
        data.put("thisMonthOrderNumber",thisMonthOrderNumber);
        //thisMonthVisitsNumber :0,
        Integer thisMonthVisitsNumber = orderDao.findVisitsCountAfterDate(firstDayThisMonth);
        data.put("thisMonthVisitsNumber",thisMonthVisitsNumber);
        //hotSetmeal :[
        //            {name:'阳光爸妈升级肿瘤12项筛查（男女单人）体检套餐',setmeal_count:200,proportion:0.222},
        //            {name:'粉红珍爱(女)升级TM12项筛查体检套餐',setmeal_count:200,proportion:0.222}
        //            ]
        List<Map> hotSetmeal = orderDao.findHotSetmeal();
        data.put("hotSetmeal",hotSetmeal);
        return data;
    }
}
