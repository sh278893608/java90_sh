package com.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.health.dao.PermissionDao;
import com.health.entity.PageResult;
import com.health.entity.QueryPageBean;
import com.health.pojo.Permission;
import com.health.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service(interfaceClass = PermissionService.class)
@Transactional
public class PermissionServiceImpl implements PermissionService {

    @Autowired
    private PermissionDao permissionDao;

    @Override
    public PageResult queryPage(QueryPageBean queryPageBean) {
        Integer currentPage = queryPageBean.getCurrentPage();
        Integer pageSize = queryPageBean.getPageSize();
        String queryString = queryPageBean.getQueryString();
        //开启分页助手
        PageHelper.startPage(currentPage,pageSize);
        //查询数据
        Page<Permission> page = permissionDao.findByQueryString(queryString);
        List<Permission> permissionList = page.getResult();
        long total = page.getTotal();
        PageResult pageResult = new PageResult(total,permissionList);
        return pageResult;
    }

    @Override
    public void add(Permission permission) {
        permissionDao.add(permission);
    }

    @Override
    public void delete(Integer id) {
        Long count = permissionDao.findRoleCountById(id);
        if (count>0){
            //权限对应有角色
            throw new RuntimeException();
        }else {
            permissionDao.delete(id);
        }
    }

    @Override
    public Permission findOne(Integer id) {
        Permission permission = permissionDao.findOne(id);
        return permission;
    }

    @Override
    public void edit(Permission permission) {
        permissionDao.edit(permission);
    }

    @Override
    public List<Permission> findAll() {
        return permissionDao.findAll();
    }

    @Override
    public List<Integer> findPermissionIdsByRoleId(Integer roleId) {
        return permissionDao.findPermissionIdsByRoleId(roleId);
    }
}
