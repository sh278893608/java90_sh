package com.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.health.constant.RedisConstant;
import com.health.dao.CheckGroupDao;
import com.health.dao.CheckItemDao;
import com.health.dao.SetMealDao;
import com.health.entity.PageResult;
import com.health.entity.QueryPageBean;
import com.health.pojo.CheckGroup;
import com.health.pojo.CheckItem;
import com.health.pojo.Setmeal;
import com.health.service.SetMealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import redis.clients.jedis.JedisPool;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = SetMealService.class)
@Transactional
public class SetMealServiceImpl implements SetMealService {

    @Autowired
    private SetMealDao setMealDao;

    @Autowired
    private JedisPool jedisPool;

    @Autowired
    private CheckGroupDao checkGroupDao;

    @Autowired
    private CheckItemDao checkItemDao;

    @Override
    public void add(Setmeal setmeal, Integer[] checkgroupIds) {
        setMealDao.add(setmeal);
        jedisPool.getResource().sadd(RedisConstant.SETMEAL_PIC_DB_RESOURCES,setmeal.getImg());
        Integer setmealId = setmeal.getId();
        //设置套餐、检查组关联
        setMealGroup(setmealId,checkgroupIds);

    }

    @Override
    public PageResult findAll(QueryPageBean queryPageBean) {
        Integer currentPage = queryPageBean.getCurrentPage();
        Integer pageSize = queryPageBean.getPageSize();
        String queryString = queryPageBean.getQueryString();
        //开启分页助手
        PageHelper.startPage(currentPage,pageSize);
        //获取分页数据
        Page<Setmeal> page = setMealDao.findAll(queryString);
        PageResult pageResult = new PageResult(page.getTotal(), page.getResult());
        return pageResult;
    }

    @Override
    public boolean confirmRel(Integer setmealId) {
        Integer count = setMealDao.confirmRel(setmealId);
        if (count>0){
            return true;
        }else {
            return false;
        }
    }

    @Override
    public void delete(Integer setmealId) {
        setMealDao.deleteRel(setmealId);
        Setmeal setmeal = setMealDao.findById(setmealId);
        jedisPool.getResource().srem(RedisConstant.SETMEAL_PIC_DB_RESOURCES,setmeal.getImg());
        setMealDao.delete(setmealId);

    }

    @Override
    public List<Setmeal> findAllMob() {
        List<Setmeal> list = setMealDao.findAllMob();
        return list;
    }

    @Override
    public Setmeal findSetmealById(Integer id) {
        Setmeal setmeal = setMealDao.findById(id);
        List<Integer> cgIds = checkGroupDao.getCgIds(id);
        List<CheckGroup> checkGroupList = new ArrayList<>();
        for (Integer cgId : cgIds) {
            CheckGroup checkGroup = checkGroupDao.findById(cgId);
            List<CheckItem> checkItemList = new ArrayList<>();
            List<Integer> ciIds = checkItemDao.getCiIds(cgId);
            for (Integer ciId : ciIds) {
                CheckItem checkItem = checkItemDao.findOne(ciId);
                checkItemList.add(checkItem);
            }
            checkGroup.setCheckItems(checkItemList);
            checkGroupList.add(checkGroup);
        }
        setmeal.setCheckGroups(checkGroupList);
        return setmeal;
    }

    @Override
    public List<Map<String, Object>> findSetmealCount() {
        return setMealDao.findSetmealCount();
    }

    @Override
    public List<Setmeal> findAllSetmeal() {
        return setMealDao.findAllSetmeal();
    }

    public void setMealGroup(Integer setmealId,Integer[] checkgroupIds){
        if (checkgroupIds!=null&&checkgroupIds.length>0){
            for (Integer checkgroupId : checkgroupIds) {
                Map<String,Integer> map = new HashMap<>();
                map.put("checkgroupId",checkgroupId);
                map.put("setmealId",setmealId);
                setMealDao.setMealGroup(map);
            }
        }
    }
}
