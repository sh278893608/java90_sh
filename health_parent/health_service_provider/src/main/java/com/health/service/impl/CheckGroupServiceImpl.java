package com.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.health.dao.CheckGroupDao;
import com.health.entity.PageResult;
import com.health.entity.QueryPageBean;
import com.health.pojo.CheckGroup;
import com.health.service.CheckGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = CheckGroupService.class)
@Transactional
public class CheckGroupServiceImpl implements CheckGroupService {

    @Autowired
    private CheckGroupDao checkGroupDao;

    @Override
    public void add(CheckGroup checkGroup, Integer[] checkItemIds) {
        checkGroupDao.add(checkGroup);

        Integer checkGroupId = checkGroup.getId();

        if (checkItemIds!=null&&checkItemIds.length>0){
            for (Integer checkItemId : checkItemIds) {
                Map<String,Integer> map = new HashMap<>();
                map.put("checkGroupId",checkGroupId);
                map.put("checkItemId",checkItemId);
                checkGroupDao.setGroupItemRel(map);
            }
        }

    }

    @Override
    public PageResult queryPage(QueryPageBean queryPageBean) {
        Integer currentPage = queryPageBean.getCurrentPage();
        Integer pageSize = queryPageBean.getPageSize();
        String queryString = queryPageBean.getQueryString();
        PageHelper.startPage(currentPage,pageSize);

        Page<CheckGroup> page = checkGroupDao.queryPage(queryString);

        long total = page.getTotal();
        List<CheckGroup> checkGroups = page.getResult();
        PageResult pageResult = new PageResult(total, checkGroups);
        return pageResult;
    }

    @Override
    public CheckGroup findById(Integer id) {
        CheckGroup checkGroup = checkGroupDao.findById(id);
        return checkGroup;
    }

    @Override
    public List<Integer> findCheckItemIdsByCheckGroupId(Integer checkgroup_id) {
        List<Integer> checkItemIds = checkGroupDao.findCheckItemIdsByCheckGroupId(checkgroup_id);
        return checkItemIds;
    }

    @Override
    public void edit(CheckGroup checkGroup, Integer[] checkitemIds) {
        checkGroupDao.edit(checkGroup);

        Integer checkGroupId = checkGroup.getId();
        checkGroupDao.deleteRel(checkGroupId);

        if (checkitemIds!=null){
            for (Integer checkitemId : checkitemIds) {
                HashMap<String, Integer> map = new HashMap<>();
                map.put("checkItemId",checkitemId);
                map.put("checkGroupId",checkGroupId);
                checkGroupDao.setGroupItemRel(map);
            }
        }
    }

    @Override
    public boolean confirmRel(Integer checkGroupId) {
        Long count = checkGroupDao.confirmRel(checkGroupId);
        if (count>0){
            return true;
        }else {
            return false;
        }
    }

    @Override
    public void deleteRel(Integer checkGroupId) {
        checkGroupDao.deleteRel(checkGroupId);
        checkGroupDao.delete(checkGroupId);
    }

    @Override
    public List<CheckGroup> findAll() {
        return checkGroupDao.findAll();
    }
}
