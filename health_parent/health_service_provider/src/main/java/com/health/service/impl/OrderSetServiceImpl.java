package com.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.health.dao.OrderSetDao;
import com.health.entity.PageResult;
import com.health.entity.QueryPageBean;
import com.health.pojo.OrderSetting;
import com.health.service.OrderSetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service(interfaceClass = OrderSetService.class)
@Transactional
public class OrderSetServiceImpl implements OrderSetService {

    @Autowired
    private OrderSetDao orderSetDao;

    @Override
    public void add(List<OrderSetting> orderSetList) {
        for (OrderSetting orderSetting : orderSetList) {
            Date orderDate = orderSetting.getOrderDate();
            Long count = orderSetDao.findByOrderDate(orderDate);
            if (count>0){
                orderSetDao.editNumByOrderDate(orderSetting);
            }else {
                orderSetDao.add(orderSetting);
            }
        }
    }

    @Override
    public List<Map> getOrderSetsByMouth(String date) {
        String begin = date + "-01";
        String end = date + "-31";
        Map<String,String> conditionMap = new HashMap<>();
        conditionMap.put("begin",begin);
        conditionMap.put("end",end);
        List<OrderSetting> selectList = orderSetDao.getOrderSetsByMouth(conditionMap);
        //得到泛型为map的list
        List<Map> orderSetsList = new ArrayList<>();
        for (OrderSetting orderSetting : selectList) {
            Date orderDate = orderSetting.getOrderDate();
            int number = orderSetting.getNumber();
            int reservations = orderSetting.getReservations();
            Map<String,Object> map = new HashMap<>();
            map.put("date",orderDate.getDate());
            map.put("number",number);
            map.put("reservations",reservations);
            orderSetsList.add(map);
        }
        return orderSetsList;
    }

    @Override
    public void editOne(OrderSetting orderSetting) {
        Date orderDate = orderSetting.getOrderDate();
        Long count = orderSetDao.findByOrderDate(orderDate);
        if (count>0){
            orderSetDao.editNumByOrderDate(orderSetting);
        }else {
            orderSetDao.add(orderSetting);
        }
    }


}
