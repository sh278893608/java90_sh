package com.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.health.dao.PermissionDao;
import com.health.dao.RoleDao;
import com.health.dao.UserDao;
import com.health.entity.PageResult;
import com.health.entity.QueryPageBean;
import com.health.pojo.Permission;
import com.health.pojo.Role;
import com.health.pojo.User;
import com.health.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service(interfaceClass = UserService.class)
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private PermissionDao permissionDao;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public User findByUsername(String username) {
        User user = userDao.findByUsername(username);
        if (user==null){
            return null;
        }
        //凭userId查角色
        Integer userId = user.getId();
        Set<Role> roles = roleDao.findByUserId(userId);
        //遍历roles通过roleId查权限
        for (Role role : roles) {
            Integer roleId = role.getId();
            Set<Permission> permissions = permissionDao.findByRoleId(roleId);
            role.setPermissions(permissions);
        }
        user.setRoles(roles);
        return user;
    }

    @Override
    public PageResult queryPage(QueryPageBean queryPageBean) {

        Integer currentPage = queryPageBean.getCurrentPage();   //当前页
        Integer pageSize = queryPageBean.getPageSize();         //每页显示的条数
        String queryString = queryPageBean.getQueryString();    //查询的关键字

        PageHelper.startPage(currentPage, pageSize);            //设置分页查询条件

        Page<User> userPage = userDao.selectByCondition(queryString); //根据条件查询用户

        //封装总页数和查询到的用户记录，返回到控制层
        PageResult pageResult = new PageResult(userPage.getTotal(), userPage.getResult());
        return pageResult;
    }

    @Override
    public void add(User user) {
        //新建用户，加密用户密码
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userDao.add(user);
    }

    @Override
    public void updateRoles(Integer userId, Integer[] roleIds) {
        //先删除用户对应的角色
        deleteRoles(userId);
        //再遍历角色，分条插入到用户角色关系表中
        for (Integer roleId : roleIds) {
            //使用map集合存储用户id和角色id，方便代入SQL语句
            Map<String, Integer> map = new HashMap<>();
            map.put("userId",userId );
            map.put("roleId",roleId );
            userDao.updateRoles(map);
        }
    }

    @Override
    public void deleteUser(Integer userId) {
        deleteRoles(userId);
        //再删除用户表的数据
        userDao.deleteUser(userId);

    }

    private void deleteRoles(Integer userId) {
        //先查询是否有关联数据
        Long count = roleDao.findUserIdAndRoleId(userId);
        if (count>0){
            //删除用户先删除用户关联的关系表数据
            roleDao.deleteByUserId(userId);
            //再删除用户表的数
        }
    }
}
