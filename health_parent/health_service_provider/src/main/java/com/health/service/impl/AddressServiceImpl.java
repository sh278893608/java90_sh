package com.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.health.dao.AddressDao;
import com.health.entity.PageResult;
import com.health.entity.QueryPageBean;
import com.health.pojo.Address;
import com.health.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
@Service(interfaceClass = AddressService.class)
public class AddressServiceImpl implements AddressService {
    @Autowired
    private AddressDao addressDao;

    //新增地址
    @Override
    public void add(Address address) {
        addressDao.add(address);
    }

   //删除地址
    @Override
    public void del(Integer id) {
        addressDao.del(id);
    }

    //检查项分页查询
    public PageResult findAll(QueryPageBean queryPageBean) {
        Integer currentPage = queryPageBean.getCurrentPage();
        Integer pageSize = queryPageBean.getPageSize();
        String queryString = queryPageBean.getQueryString();//查询条件
        //完成分页查询，基于mybatis框架提供的分页助手插件完成
        PageHelper.startPage(currentPage,pageSize);
        //select * from t_address limit 0,10
        Page<Address> page = addressDao.selectByCondition(queryString);
        long total = page.getTotal();
        List<Address> rows = page.getResult();
        return new PageResult(total,rows);
    }

    @Override
    public List<String> findAllAddr() {
        return addressDao.findAllAddr();
    }
}
